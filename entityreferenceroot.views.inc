<?php

/**
 * @file
 * Contains entityreferenceroot\entityreferenceroot.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Render\Markup;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\system\ActionConfigEntityInterface;

/**
* Implements hook_views_data().
*/
function entityreferenceroot_views_data() {

    $data['views']['table']['group'] = t('Custom Global');
    $data['views']['table']['join'] = [
      // #global is a special flag which allows a table to appear all the time.
      '#global' => [],
    ];

    $data['views']['entity_reference_root_views_field'] = [
        'title' => t('Entity Reference Root'),
        'help' => t('Find the root elements of a node.'),
        'field' => [
            'id' => 'entity_reference_root_views_field',
        ],
    ];

    return $data;
}
