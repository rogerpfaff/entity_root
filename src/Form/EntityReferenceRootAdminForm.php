<?php

namespace Drupal\entityreferenceroot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class EntityReferenceRootAdminForm.
 */
class EntityReferenceRootAdminForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Constructs a new EntityReferenceRootAdminForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_reference_root_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Check all reference fields for the target entities and set them as option
    $ref_fields = $this->entityTypeManager->getStorage('field_storage_config')->loadbyProperties(
      [
        'settings' => ['target_type' => TRUE],
      ]
    );

    foreach ($ref_fields as $ref_field) {
      // TODO: Get the label of the targeted entity type
      $options[$ref_field->getSetting('target_type')] = $ref_field->getSetting('target_type');
    }

    $reference_types = $this->config('entityreferenceroot.entityreferenceroot_settings')->get('entityreferenceroot_reference_types');

    $form['entityreferenceroot_reference_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the referenced entity types to follow.'),
      '#options' => $options,
      '#default_value' => $reference_types
    ];

    // Get all the node types for the root selection
    $options = null;
    // Get the root node types
    $root_types = $this->config('entityreferenceroot.entityreferenceroot_settings')->get('entityreferenceroot_root_types');
    // Get available content types.
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // Generate a checkbox for each content type
    foreach ($node_types as $node_type) {
      $options[$node_type->get('type')] = $node_type->label();
    }
    $form['entityreferenceroot_root_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the root node types.'),
      '#options' => $options,
      '#default_value' => $root_types
    ];

    
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValue('entityreferenceroot_reference_types') as $key => $value) {
      if ($value) {
        $values[$key] = $value;
      }
    }
    $this->configFactory->getEditable('entityreferenceroot.entityreferenceroot_settings')
      ->set('entityreferenceroot_reference_types', $values)
      ->save();

    $values = null;
    foreach ($form_state->getValue('entityreferenceroot_root_types') as $key => $value) {
      if ($value) {
        $values[$key] = $value;
      }
    }
    $this->configFactory->getEditable('entityreferenceroot.entityreferenceroot_settings')
      ->set('entityreferenceroot_root_types', $values)
      ->save();

    \Drupal::messenger()->addMessage('Configuration saved.');
  }

}
