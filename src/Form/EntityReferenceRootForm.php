<?php

namespace Drupal\entityreferenceroot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\entityreferenceroot\EntityReferenceRootBuilder;
use Drupal\entityreferenceroot\EntityReferenceTreeBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class EntityReferenceRootForm.
 */
class EntityReferenceRootForm extends FormBase {

  /**
   * @var \Drupal\node\NodeStorage
   */
  protected $node_storage;
  /**
   * @var EntityReferenceRootBuilder
   */
  protected $rootBuilder;

  /**
   * Constructs a new EntityReferenceRootForm object.
   */
  public function __construct(
    EntityReferenceRootBuilder $root_builder,
    EntityStorageInterface $node_storage
  ) {
    $this->rootBuilder = $root_builder;
    $this->node_storage = $node_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entityreferenceroot.builder'),
      $container->get('entity_type.manager')->getStorage('node')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_reference_root_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => 'Clear and rebuild the reference root relations.'
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Build'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $batch = [
      'title' => 'Building reference root relations.',
      'operations' => [
      ],
      'finished' => '\Drupal\entityreferenceroot\EntityreferenceRootBuilder::finishedBatchCallback'
    ];

    foreach ($this->rootBuilder->getRootNodes() as $rootNode) {
      $batch['operations'][] = ['\Drupal\entityreferenceroot\EntityReferenceRootBuilder::buildRootRelation', [$rootNode]];
    }
    $this->rootBuilder->clearRootRelations();
    batch_set($batch);
  }

}
