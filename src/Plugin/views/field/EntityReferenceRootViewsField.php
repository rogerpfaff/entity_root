<?php

namespace Drupal\entityreferenceroot\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Link;
use Drupal\Core\Url;
/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("entity_reference_root_views_field")
 */
class EntityReferenceRootViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $return = [];
    $connection = \Drupal::database();
    $result = $connection->query('SELECT root FROM {entityreferenceroot} WHERE nid = :nid', [':nid' => $values->node_field_data_nid])
      ->fetchAll();
    foreach ($result as $row) {
      $link = Link::fromTextAndUrl($row->root, Url::fromRoute('entity.node.canonical', ['node' => $row->root]));
      $links[] = $link->toRenderable();
      $return = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $links,
        '#attributes' => ['class' => 'entityreferenceroot_list'],
        '#wrapper_attributes' => ['class' => 'container'],
      ];
    }
    return $return;
  }

}
