<?php

namespace Drupal\entityreferenceroot;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

use Drupal\node\Entity\Node;

/**
 * Class EntityReferenceRootBuilder.
 */
class EntityReferenceRootBuilder {
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * Constructs a new EntityReferenceRootBuilder object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityStorage = $entity_type_manager->getStorage('node');
    $this->configFactory = $config_factory;
  }

  public function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory')
    );
  }

  public static function buildRootRelation(Node $root_node, &$context) {
    if (!$root_node) {
      return;
    }
    $tree_builder = \Drupal::service('entityreferenceroot.tree_builder');
    $connection = \Drupal::database();

    foreach ($tree_builder->buildReferenceTree($root_node) as $child) {
      if (!$connection->query('SELECT * FROM {entityreferenceroot} WHERE nid = :nid AND root = :root',
        [':nid' => $child->id(), ':root' => $root_node->id()])->fetchField()) {
        $query = $connection->insert('entityreferenceroot')
          ->fields([
            'nid' => $child->id(),
            'root' => $root_node->id(),
            'entity_id' => $child->getEntityTypeId(),
            'entity_type_id' => $child->getType(),
          ])
          ->execute();
      }
    }
  }

  public static function finishedBatchCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Tree processed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

  public function getRootNodes() {
    $tree_builder = \Drupal::service('entityreferenceroot.tree_builder');
    $root_types = \Drupal::config('entityreferenceroot.entityreferenceroot_settings')->get('entityreferenceroot_root_types');
    $root_types = array_keys($root_types);
    $nodes = $this->entityStorage->getQuery()
      ->condition('type', $root_types, 'IN')
      ->execute();
    $nodes = array_values($nodes);
    $i = 0;
    while ($nid = $nodes[$i]) {
      yield $this->entityStorage->load($nid);
      $i++;
    }
  }

  public function clearRootRelations() {
    $connection = \Drupal::database();
    $query = $connection->truncate('entityreferenceroot')->execute();
  }

}
