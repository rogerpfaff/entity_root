<?php
/**
 * Created by PhpStorm.
 * User: rogerpfaff
 * Date: 08.10.18
 * Time: 14:45
 */

namespace Drupal\entityreferenceroot;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\node\Entity\Node;


class EntityReferenceTreeBuilder {
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * Entity Storage Instance
   */
  protected $entityStorage;
  /**
   * The mapping of content type reference fields
   *
   * @var array $entityTypeFields
   */
  public $entityTypeFields = [];
  /**
   * The references in a tree structure
   *
   * @var array $referenceTree
   */
  public $referenceTree = [];

  /**
   * Constructs a new EntityReferenceRootBuilder object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactoryInterface $config_factory
) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->configFactory = $config_factory;
    $this->setEntityTypeFields();

  }

  public function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * @param array $entityTypeFields
   */
  public function setEntityTypeFields(array $entityTypeFields = null) {
    if (!$entityTypeFields) {

      $reference_types = \Drupal::config('entityreferenceroot.entityreferenceroot_settings')->get('entityreferenceroot_reference_types');

      foreach ($reference_types as $reference_type) {
        $field_map_references = $this->entityFieldManager->getFieldMapByFieldType('entity_reference');
        $field_map_references = array_merge_recursive($field_map_references, $this->entityFieldManager->getFieldMapByFieldType('entity_reference_revisions'));
        foreach ($field_map_references[$reference_type] as $field_name => $field_map_reference) {
          if (strstr($field_name, 'field_')) {
            foreach ($field_map_reference['bundles'] as $bundle) {
              $this->entityTypeFields[$reference_type][$bundle][] = $field_name;
            }
          }
        }
        // TODO: Handle file entity_types. When are they fielded?
      }
    }
  }

  public function getEntityTypeInfo($entity_id) {
    $field_map_references = $this->entityFieldManager->getFieldMapByFieldType('entity_reference');
    foreach ($field_map_references[$entity_id] as $field_name => $reference_settings) {


      if (strstr($field_name, 'field_')) {
        foreach ($reference_settings['bundles'] as $bundle) {
          $this->entityTypeFields[$reference_settings][$bundle][] = $field_name;
        }
      }
    }

  }

  public function buildReferenceTree($entity) {
    foreach ($this->getReferencedEntities($entity) as $reference) {
      yield $reference;
      yield from $this->buildReferenceTree($reference);
    }
  }

  public function getReferencedEntities(Entity $entity) {
    $root_types = \Drupal::config('entityreferenceroot.entityreferenceroot_settings')->get('entityreferenceroot_root_types');
    $this->entityStorage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    foreach ($this->entityTypeFields[$entity->getEntityTypeId()][$entity->getType()] as $field) {
      foreach ($entity->getTranslationLanguages() as $langcode => $language) {
        $translation = $entity->getTranslation($langcode);
        $values = $translation->get($field)->getValue();
        // the node has fields but no referenced nodes
        // add as endpoint
        if (empty($values)) {
          continue;
        }
        $reference_ids = array_column($values, 'target_id');
        foreach ($values as $value) {
          $reference_ids[] = $value['target_id'];
        }
        // get the right entity storage from the field
        $definition = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->getType());
        $handler = $definition[$field]->getSetting('handler');
        $reference_entities = $this->entityStorage->loadMultiple($reference_ids);
        foreach ($reference_entities as $reference_entity) {
          // if the referenced entity is deleted set current node as endpoint
          if ($reference_entity === NULL) {
            continue;
          }
          // The referenced node is a root type
          // set current node as endpoint
          if (isset($root_types[$reference_entity->getType()])) {
            continue;
          }
          yield $reference_entity;

        }
      }
    }
  }

  function getReferencedNodes(Node $node) {
    $root_types = \Drupal::config('entityreferenceroot.entityreferenceroot_settings')->get('entityreferenceroot_root_types');
    foreach ($this->entityTypeFields['node'][$node->getType()] as $field) {
      foreach ($node->getTranslationLanguages() as $langcode => $language) {
        $translation = $node->getTranslation($langcode);
        $values = $translation->get($field)->getValue();
        // the node has fields but no referenced nodes
        // add as endpoint
        if (empty($values)) {
          continue;
        }
        $reference_nids = array_column($values, 'target_id');
        foreach ($values as  $value) {
          $reference_nids[] = $value['target_id'];
        }
        $reference_nodes = $this->entityStorage->loadMultiple($reference_nids);
        foreach ($reference_nodes as $reference_node) {
          // if the referenced node is deleted set current node as endpoint
          if ($reference_node === NULL) {
            continue;
          }
          // The referenced node is a root type
          // set current node as endpoint
          if (isset($root_types[$reference_node->getType()])) {
            continue;
          }
          yield $reference_node;
        }
      }
    }
  }
}